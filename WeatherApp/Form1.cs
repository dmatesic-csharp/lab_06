namespace WeatherApp {
    public partial class Form1 : Form {
        private Vrijeme vrijeme;

        public Form1() {
            InitializeComponent();
            vrijeme = new Vrijeme();
            OsvjeziPopisGradova();
            OsvjeziListe();
        }

        private void OsvjeziPopisGradova() {
            cbGradovi.Items.Clear();
            cbGradovi.Items.AddRange(vrijeme.GetGradovi().ToArray());
        }

        private void PrikazPodataka(string grad) {
            var podaci = vrijeme.GetPodaci(grad);

            lblGrad.Text = podaci.Grad;
            lblTemperatura.Text = podaci.TemperaturaHuman;
            lblVlaga.Text = podaci.VlagaHuman;
            lblTlak.Text = podaci.TlakHuman;
        }

        private void OsvjeziListe() {
            lbNajtopliji.Items.Clear();
            lbNajhladniji.Items.Clear();

            lbNajtopliji.Items.AddRange(vrijeme.GetTopliGradovi().ToArray());
            lbNajhladniji.Items.AddRange(vrijeme.GetHladniGradovi().ToArray());
        }

        private void cbGrad_SelectedIndexChanged(object sender, EventArgs e) {
            PrikazPodataka(cbGradovi.Text);
        }
    }
}