﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApp {
    public class VrijemePodatak {
        public VrijemePodatak(string grad, string temperatura, string vlaga, string tlak) {
            Grad = grad;
            Temperatura = double.Parse(PripremiString(temperatura));
            Vlaga = int.Parse(PripremiString(vlaga));
            Tlak = double.Parse(PripremiString(tlak));
        }

        public override string ToString() {
            return $@"{Grad}, {TemperaturaHuman}";
        }

        private string PripremiString(string unos) {
            return unos.Replace("*", "").Replace("-", "0").Replace(".", ",");
        }

        public string TemperaturaHuman { get => Temperatura + " °C"; }
        public string VlagaHuman { get => Vlaga + " %"; }
        public string TlakHuman { get => Tlak + " hPa"; }
        public string Grad { get; set; }
        public double Temperatura { get; set; }
        public int Vlaga { get; set; }

        public double Tlak { get; set; }

    }
}
