﻿namespace WeatherApp {
    partial class Form1 {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.cbGradovi = new System.Windows.Forms.ComboBox();
            this.lblGrad = new System.Windows.Forms.Label();
            this.lblTemperatura = new System.Windows.Forms.Label();
            this.lblVlaga = new System.Windows.Forms.Label();
            this.lblTlak = new System.Windows.Forms.Label();
            this.scKontejner = new System.Windows.Forms.SplitContainer();
            this.lbNajtopliji = new System.Windows.Forms.ListBox();
            this.lblNajtopliji = new System.Windows.Forms.Label();
            this.lbNajhladniji = new System.Windows.Forms.ListBox();
            this.lblNajhladniji = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.scKontejner)).BeginInit();
            this.scKontejner.Panel1.SuspendLayout();
            this.scKontejner.Panel2.SuspendLayout();
            this.scKontejner.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbGradovi
            // 
            this.cbGradovi.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbGradovi.FormattingEnabled = true;
            this.cbGradovi.Location = new System.Drawing.Point(14, 16);
            this.cbGradovi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbGradovi.Name = "cbGradovi";
            this.cbGradovi.Size = new System.Drawing.Size(567, 28);
            this.cbGradovi.TabIndex = 0;
            this.cbGradovi.Text = "Odaberite grad";
            this.cbGradovi.SelectedIndexChanged += new System.EventHandler(this.cbGrad_SelectedIndexChanged);
            // 
            // lblGrad
            // 
            this.lblGrad.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGrad.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblGrad.Location = new System.Drawing.Point(14, 85);
            this.lblGrad.Name = "lblGrad";
            this.lblGrad.Size = new System.Drawing.Size(568, 59);
            this.lblGrad.TabIndex = 1;
            this.lblGrad.Text = "(grad nije odabran)";
            this.lblGrad.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTemperatura
            // 
            this.lblTemperatura.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTemperatura.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTemperatura.Location = new System.Drawing.Point(14, 144);
            this.lblTemperatura.Name = "lblTemperatura";
            this.lblTemperatura.Size = new System.Drawing.Size(568, 59);
            this.lblTemperatura.TabIndex = 2;
            this.lblTemperatura.Text = "? °C";
            this.lblTemperatura.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblVlaga
            // 
            this.lblVlaga.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVlaga.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblVlaga.Location = new System.Drawing.Point(14, 203);
            this.lblVlaga.Name = "lblVlaga";
            this.lblVlaga.Size = new System.Drawing.Size(568, 59);
            this.lblVlaga.TabIndex = 3;
            this.lblVlaga.Text = "? %";
            this.lblVlaga.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTlak
            // 
            this.lblTlak.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTlak.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTlak.Location = new System.Drawing.Point(14, 261);
            this.lblTlak.Name = "lblTlak";
            this.lblTlak.Size = new System.Drawing.Size(568, 59);
            this.lblTlak.TabIndex = 4;
            this.lblTlak.Text = "? hPa";
            this.lblTlak.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // scKontejner
            // 
            this.scKontejner.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scKontejner.Location = new System.Drawing.Point(14, 347);
            this.scKontejner.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.scKontejner.Name = "scKontejner";
            // 
            // scKontejner.Panel1
            // 
            this.scKontejner.Panel1.Controls.Add(this.lbNajtopliji);
            this.scKontejner.Panel1.Controls.Add(this.lblNajtopliji);
            // 
            // scKontejner.Panel2
            // 
            this.scKontejner.Panel2.Controls.Add(this.lbNajhladniji);
            this.scKontejner.Panel2.Controls.Add(this.lblNajhladniji);
            this.scKontejner.Size = new System.Drawing.Size(568, 389);
            this.scKontejner.SplitterDistance = 282;
            this.scKontejner.SplitterWidth = 5;
            this.scKontejner.TabIndex = 5;
            // 
            // lbNajtopliji
            // 
            this.lbNajtopliji.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbNajtopliji.FormattingEnabled = true;
            this.lbNajtopliji.ItemHeight = 20;
            this.lbNajtopliji.Location = new System.Drawing.Point(0, 31);
            this.lbNajtopliji.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbNajtopliji.Name = "lbNajtopliji";
            this.lbNajtopliji.Size = new System.Drawing.Size(282, 358);
            this.lbNajtopliji.TabIndex = 1;
            // 
            // lblNajtopliji
            // 
            this.lblNajtopliji.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblNajtopliji.Location = new System.Drawing.Point(0, 0);
            this.lblNajtopliji.Name = "lblNajtopliji";
            this.lblNajtopliji.Size = new System.Drawing.Size(282, 31);
            this.lblNajtopliji.TabIndex = 0;
            this.lblNajtopliji.Text = "Najtopliji gradovi";
            this.lblNajtopliji.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbNajhladniji
            // 
            this.lbNajhladniji.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbNajhladniji.FormattingEnabled = true;
            this.lbNajhladniji.ItemHeight = 20;
            this.lbNajhladniji.Location = new System.Drawing.Point(0, 31);
            this.lbNajhladniji.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lbNajhladniji.Name = "lbNajhladniji";
            this.lbNajhladniji.Size = new System.Drawing.Size(281, 358);
            this.lbNajhladniji.TabIndex = 1;
            // 
            // lblNajhladniji
            // 
            this.lblNajhladniji.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblNajhladniji.Location = new System.Drawing.Point(0, 0);
            this.lblNajhladniji.Name = "lblNajhladniji";
            this.lblNajhladniji.Size = new System.Drawing.Size(281, 31);
            this.lblNajhladniji.TabIndex = 0;
            this.lblNajhladniji.Text = "Najhladniji gradovi";
            this.lblNajhladniji.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 752);
            this.Controls.Add(this.scKontejner);
            this.Controls.Add(this.lblTlak);
            this.Controls.Add(this.lblVlaga);
            this.Controls.Add(this.lblTemperatura);
            this.Controls.Add(this.lblGrad);
            this.Controls.Add(this.cbGradovi);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "WeatherApp";
            this.scKontejner.Panel1.ResumeLayout(false);
            this.scKontejner.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scKontejner)).EndInit();
            this.scKontejner.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ComboBox cbGradovi;
        private Label lblGrad;
        private Label lblTemperatura;
        private Label lblVlaga;
        private Label lblTlak;
        private SplitContainer scKontejner;
        private Label lblNajtopliji;
        private Label lblNajhladniji;
        private ListBox lbNajtopliji;
        private ListBox lbNajhladniji;
    }
}