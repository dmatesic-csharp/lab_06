﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WeatherApp {
    public class Vrijeme {
        public Vrijeme() {
            vrijeme = new XmlDocument();
            vrijeme.Load("http://vrijeme.hr/hrvatska_n.xml");

        }

        public List<VrijemePodatak> DohvatiPodatke() {

            List<VrijemePodatak> podaci = new List<VrijemePodatak>();

            XmlNodeList gradovi = vrijeme.GetElementsByTagName("Grad");
            foreach (XmlNode grad in gradovi) {
                podaci.Add(new VrijemePodatak(
                   grad["GradIme"].InnerText,
                grad["Podatci"]["Temp"].InnerText,
                grad["Podatci"]["Vlaga"].InnerText,
                grad["Podatci"]["Tlak"].InnerText
            ));

            }
            return podaci;
        }

        public IEnumerable<string> GetGradovi() {
            List<VrijemePodatak> podaci = DohvatiPodatke();
            return
              from p in podaci
              orderby p.Grad
              select p.Grad;
        }

        public VrijemePodatak GetPodaci(string grad) {
            List<VrijemePodatak> podaci = DohvatiPodatke();
            return
              (from p in podaci
               where p.Grad == grad
               select p).First();
        }

        public IEnumerable<VrijemePodatak> GetTopliGradovi() {
            List<VrijemePodatak> podaci = DohvatiPodatke();
            return
              (from p in podaci
               orderby p.Temperatura descending
               select p).Take(5);
        }

        public IEnumerable<VrijemePodatak> GetHladniGradovi() {
            List<VrijemePodatak> podaci = DohvatiPodatke();
            return
              (from p in podaci
               orderby p.Temperatura
               select p).Take(5);
        }

        private XmlDocument vrijeme;

    }
}
